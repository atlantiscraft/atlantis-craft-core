package com.atlantiscraft.core.item.crystal;

import com.atlantiscraft.core.creativetab.CreativeTabAtlantisCraftCore;
import com.atlantiscraft.core.creativetab.CreativeTabAtlantisCraftCoreCrystals;
import com.atlantiscraft.core.item.ItemAtlantisCraft;

public class ItemRawCrystaline extends ItemAtlantisCraft{

    public ItemRawCrystaline(){

        super();
        this.setUnlocalizedName("rawCrystaline");
        this.setCreativeTab(CreativeTabAtlantisCraftCoreCrystals.AtlantisCraftCoreCrystalsTab);
    }
}
