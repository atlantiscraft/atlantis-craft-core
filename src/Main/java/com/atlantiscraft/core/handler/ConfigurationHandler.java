package com.atlantiscraft.core.handler;

import com.atlantiscraft.core.reference.Configurations;
import com.atlantiscraft.core.reference.Reference;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class ConfigurationHandler {

    public static Configuration configuration;

    public static void init(File configFile){

        if(configuration == null) {
            configuration = new Configuration(configFile);
            loadConfiguration();

        }

    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event){

        if(event.modID.equalsIgnoreCase(Reference.MOD_ID)){

            loadConfiguration();
        }

    }

    private static void loadConfiguration(){

        Configurations.crystalForestGen = configuration.get(Configuration.CATEGORY_GENERAL,"crystalForestGen",true,"If you would like to have crystal forests generate in your world, leave true").getBoolean(true);
        Configurations.easyMode = configuration.get(Configuration.CATEGORY_GENERAL,"easyMode",false,"If you would like to play Atlantis Craft as we intended it, leave this false").getBoolean(false);
        Configurations.oreGen = configuration.get(Configuration.CATEGORY_GENERAL,"oreGen",true,"Leave this as true if you would like ores to generate in your world").getBoolean(true);
        Configurations.zeroPointModuleCraftable = configuration.get(Configuration.CATEGORY_GENERAL,"zeroPointModuleCraftable",true,"If you would like to disable the crafting of an EXTREMELY EXPENSIVE, MASSIVELY POWERFUL energy source, change this to false").getBoolean(true);
        Configurations.zeroPointModuleEnabled = configuration.get(Configuration.CATEGORY_GENERAL,"zeroPointModuleEnabled",true,"If you would like to disable the EXTREMELY EXPENSIVE, MASSIVELY POWERFUL energy source, change this to false (Note: If you set this to false, also set the crafting of it to false as well, otherwise you leave in a very expensive rock recipe.)").getBoolean(true);

        if(configuration.hasChanged()) {

            configuration.save();
        }


    }
}
