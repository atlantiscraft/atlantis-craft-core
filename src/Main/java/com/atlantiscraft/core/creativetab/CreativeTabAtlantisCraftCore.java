package com.atlantiscraft.core.creativetab;

import com.atlantiscraft.core.init.ModItems;
import com.atlantiscraft.core.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabAtlantisCraftCore {

    public static final CreativeTabs AtlantisCraftCoreTab = new CreativeTabs(Reference.MOD_ID) {
        @Override
        public Item getTabIconItem() {

            return ModItems.rawCrystaline;

        }

        @Override
        public String getTranslatedTabLabel(){

            return "Atlantis Craft Core";
        }
    };
}
