package com.atlantiscraft.core.init;

import com.atlantiscraft.core.item.ItemAtlantisCraft;
import com.atlantiscraft.core.item.crystal.ItemRawCrystaline;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModItems {

    public static final ItemAtlantisCraft rawCrystaline = new ItemRawCrystaline();


    public static void init(){

        GameRegistry.registerItem(rawCrystaline,"Raw Crystaline");
    }
}
