package com.atlantiscraft.core.reference;

public class Configurations {

    public static boolean crystalForestGen;
    public static boolean easyMode;
    public static boolean oreGen;
    public static boolean zeroPointModuleCraftable;
    public static boolean zeroPointModuleEnabled;

}
