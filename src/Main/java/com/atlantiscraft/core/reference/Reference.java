package com.atlantiscraft.core.reference;

public class Reference
{
    public static final String MOD_ID = "AtlantisCraftCore";
    public static final String MOD_NAME = "Atlantis Craft Core";
    public static final String VERSION = "1.7.10-1.0.0";
    public static final String CLIENT_PROXY_CLASS = "com.atlantiscraft.core.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.atlantiscraft.core.proxy.ServerProxy";
    public static final String GUI_FACTORY_CLASS = "com.atlantiscraft.core.client.gui.GuiFactory";
}